Add the following line to sites settings.php:

    $settings['page_cache_without_database'] = TRUE;

Add the following snipped to sites services.yml:

    services:
      cache.backend.mock:
        class: Drupal\mockcache\MockCacheFactory
      cache.render:
        class: Drupal\Core\Cache\CacheBackendInterface
        tags:
          - { name: cache.bin, default_backend: cache.backend.mock }
        factory_method: get
        factory_service: cache_factory
        arguments: [render]
