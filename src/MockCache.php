<?php

namespace Drupal\mockcache;

use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\HttpFoundation\Response;

class MockCache implements CacheBackendInterface {

  public function get($cid, $allow_invalid = FALSE) {
    if (substr($cid, 0, 4) === 'http') {
      return (object) [
        'data' => new Response('Hi!'),
        ];
    }
  }

  public function getMultiple(&$cids, $allow_invalid = FALSE) {}
  public function set($cid, $data, $expire = Cache::PERMANENT, array $tags = array()) {}
  public function setMultiple(array $items) {}
  public function delete($cid) {}
  public function deleteMultiple(array $cids) {}
  public function deleteAll() {}
  public function invalidate($cid) {}
  public function invalidateMultiple(array $cids) {}
  public function invalidateAll() {}
  public function garbageCollection() {}
  public function removeBin() {}

}
