<?php

namespace Drupal\mockcache;

use Drupal\Core\Cache\CacheFactoryInterface;

class MockCacheFactory implements CacheFactoryInterface {

  public function get($bin) {
    return new MockCache();
  }

}
